# VSCode デバッグ実行

複数の初心者さんに Python をレクチャーする機会をいただけたのですが、繰り返し処理・分岐処理に入った時、continue やbreak で次がコードのどこへ移動するのかなど、コードの流れを追いきれていないケースが見受けられました。

VSCode を利用しているユーザーであれば、コードの流れを追いかけることが簡単にできるデバッグモードのステップ実行を利用することをおすすめします。

今回そのような初心者ユーザーのために、自著「[Pythonに興味はあるけれどもはじめの一歩をなかなか踏み出せないでいる人のためのPython「超基礎編」](https://amzn.to/2XmNxy5)」よりVSCode のデバッグモードの記事部分だけを切り出して PDFにしました。

コードの流れを追うのが苦手だなと感じる方は、ぜひデバッグモードのステップ実行を試してみてください。
ステップインの威力を知ると病みつきになりますよ！

PDF は以下のリンクから無料で入手できます。  
https://gitlab.com/ysugimura_it/python_publish/-/raw/main/debug_mode.pdf

もし興味がありましたら Kindleで販売している著書の方も試し読みいただけると嬉しいです。  
価格は250円で Kindle Unlimited 対応なので Prime Reading 会員なら無料で読めます。  
書籍URL：https://amzn.to/2XmNxy5
